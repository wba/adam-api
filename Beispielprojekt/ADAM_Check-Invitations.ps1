function Check-Invitations
{
    # Setzen der Parameter für die Funktionen
    [CmdletBinding()]
    param
    (
        # Pfad zur Excel-Datei
        $xlsxpath,

        # Präfix der gewünschten Domäne
        $prefix
    )

    # Importieren der Excel-Datei mit den Accounts
    $File = Import-Excel -Path $xlsxpath

    # Importieren aller Benutzer aus ADAM
    $EnabledUsers = $Client.GetEnabledUsers($prefix) | Select UPN,GivenName,Sn,DisplayName,AccountType, SAMAccountName
    $DisabledUsers = $Client.GetDisabledUsers($prefix) | Select UPN,GivenName,Sn,DisplayName,AccountType, SAMAccountName
    $AllUsers = $EnabledUsers + $DisabledUsers

    # Importieren aller Invitations aus ADAM
    $AllInvitations = $Client.GetInvitations()

    foreach($AccInFile in $File)
    {
        if($AccInFile.SamAccountName -eq $null)
        {
            # Sucht nach dem jeweiligen Account in ADAM
            $Account = $AllUsers | Where {$_.UPN -eq $AccInFile.UPN}

            # Wenn der Account noch nicht existiert
            if($Account -eq $null)
            {
                # In allen vorhandenen Einladungen wird geprüft, ob der Account schon eingeladen wurde
                $AccInInvitations = $AllInvitations | Where {$_.UPN -eq $AccInFile.UPN}
                # Wenn kein Ergebnis zurückkommt, wird eine Einladung erstellt
                if($AccInInvitations -eq $null)
                {
                    Write-verbose "Der Account $($AccInFile.UPN) existiert noch nicht in der Domäne und wird nun eingeladen!"
                    $AccountType = $AccinFile.AccountType
                    $GivenName = $AccinFile.GivenName
                    $Surname   = $AccinFile.Sn
                    $UPN = $AccinFile.UPN
                    # Mehrere Gruppen müssen in der Excel-Datei mit einem Komma ohne Leerzeichen getrennt werden
                    $Groups = @($AccinFile.Groups -split ",")
                    $idmID = $AccInFile.idmID
                    
                    $invitationResult = $Client.CreateUserInvitationForExistingUser($prefix, $GivenName, $Surname, $UPN, $AccountType, $Groups, $idmID)
    
                }
                 # Wenn ein Ergebnis zurückkommt, kommt eine Statusmeldung der Einladung
                 else
                 {
                    Write-verbose "Der Account $($AccInFile.UPN) wurde am $(Get-Date $AccInInvitations.Whencreated -Format "dd.MM.yyyy HH:mm") Uhr in die Domäne eingeladen, aber die Einladung wurde noch nicht angenommen (ActivationCode: $($AccInInvitations.ActivationCode))"
                 }
            }
            # Wenn der Account schon existiert, aber noch kein SAMAccountName in der Datei eingetragen ist. (Die Einladung wurde angenommen und die Datei wird nun bearbeitet)
            else
            {
                write-verbose "Die Einladung für den Account $($AccInFile.UPN) wurde nun angenommen. Excel-Datei wird nun mit dem SAMAccountName ergänzt"
                
                # Bearbeiten des Eintrags in der Variable zum späteren Exportieren
                $AccInFile.SAMAccountName = $Account.SAMAccountName
                # Setzen des Tags zum Überschreiben der Datei
                $overwrite = 1
            }
        }
        # Wenn der SAMAccountName schon gesetzt ist
        else
        {
            Write-Verbose "Der SAMAccountName des Accounts $($AccInFile.UPN) ist schon gesetzt und der Account existiert bereits in der Domäne."
        }
    }

    # Es wird geprüft, ob das Excelfile abgeändert wurde
    if($overwrite -eq 1)
    {
        # Das abgeänderte Excelfile wird nun abgespeichert 
        # (da überschreiben nicht geht, wird das alte erst gelöscht und danach das neue erstellt)
        write-verbose "Die Excel-Datei wird nun überschrieben mit den neuen Einträgen"
        rm $xlsxpath
        $File | Export-Excel -Path $xlsxpath -AutoFilter -AutoSize
    }
    else
    {
        write-verbose 'Es mussten keine Änderungen am Excel-File vorgenommen werden. Excel-File wird nicht überschrieben'
    }
} #Ende der Funktion Check-Invitations