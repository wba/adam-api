function Connect-WebService # Verbinden mit dem WebServices
{
    # Setzen der Parameter für die Funktionen
    [CmdletBinding()]
    param($global:primarydomain="Präfix der Domäne, z.B. XYZ")

    # Get-Credentials
    $username = "srv123456@adam.rwth-aachen.de"
    $CredentialPath = "C:\Pfad\Credentials"
    $PwdSecureString = Get-Content "$CredentialPath\$Username.cred" | ConvertTo-SecureString
    $Cred = New-Object System.Management.Automation.PSCredential -ArgumentList $Username, $PwdSecureString
    
    # Mit API verbinden
    $uri = "https://adam-ws-s1.adam.rwth-aachen.de/adam-ws/ADAMAPIv01.asmx?WSDL"
    
    
    # Hier wird eine Do-Schleife verwendet, weil das verbinden mit dem Server manchmal nicht klappt
    $counter = 0
    do
    {
        $counter++
        Write-Verbose "Versuch $counter"
        $error.clear()
        $global:Client = New-WebServiceProxy -Uri $uri -Credential $cred
        if($error.length -ne 0)
        {
            sleep 2
        }
    }
    until($error.length -le "0" -or $counter -eq 5)

    if($Client -eq $null -or $counter -eq 5)
    {
        Write-host "Es konnte sich nicht mit dem ADAM-System verbunden werden, Skript wird nun abgebrochen;
        Inhalt der Error-Variable:
        $error" -foreground Red
        break
    }
    else
    {
        Write-Verbose "Verbindungsversuch zum ADAM-System erfolgreich"
    }
}