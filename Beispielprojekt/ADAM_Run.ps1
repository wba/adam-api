function Run
{
    # Setzen der Parameter für die Funktionen
    [CmdletBinding()]
    param
    (
        # Pfad zur Excel-Datei (Inputdatei)
        $xlsxpath = "C:\Verwaltungspfad\Files\BeispielExceldatei.xlsx",
        $Logpath = "C:\Verwaltungspfad\Files\Logs\$(Get-Date -format "yyyy_MM_dd__HH_mm")_Log.log",
        $prefix = ""
    )

    #Starten des Loggings
    Start-Transcript -Path $LogPath

    # Importieren der anderen Dateien
    . .\ADAM_Connect-Webservice.ps1
    . .\ADAM_Initialize.ps1
    . .\ADAM_Check-Invitations.ps1
    
    
    # Diese funktion führt automatisch alle Schritte automatisch durch in folgender Reihenfolge:       

    # Laden aller Module (Excel)
    write-host "Die Funktion Load-Modules wird ausgeführt" -Foreground darkyellow
    Initialize
    write-host "Die Funktion Load-Modules ist durchgelaufen `n" -Foreground darkyellow


    # Verbinden mit dem Webservice der RWTH Aachen
    write-host "Die Funktion Connect-Webservice wird ausgeführt" -Foreground darkyellow
    Connect-WebService
    write-host "Die Funktion Connect-Webservice ist durchgelaufen `n" -Foreground darkyellow


    # Überprüfung, ob neue Accounts existieren und ggf. Einladung erstellen
    write-host "Die Funktion Check-Invitations wird ausgeführt" -Foreground darkyellow
    Check-Invitations -xlsxpath $xlsxpath -prefix $prefix
    write-host "Die Funktion Check-Invitations ist durchgelaufen `n" -Foreground darkyellow


    # Stoppen des Loggings
    Stop-Transcript
}