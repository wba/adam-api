# ADAM API


## Hintergrund

Das IT Center der RWTH Aachen stellt den zentralen Verzeichnisdienst Active Directory für Arbeitsplatzmanagement (kurz: ADAM) bereit. Die Verwaltung des Active Directories wird durch die ADAM-Admins für die jeweiligen Einrichtungen (Domänen) über eine Web Application (ADAMAdm) vorgenommen. Um die Prozesse zur Pflege des Active Directories in die internen Abläufe automatisch integrieren zu können, wird neben dem ADAMAdm auch eine entsprechende, auf SOAP basierende, API angeboten, so dass die Pflege des Active Directories in die vorhandenen lokalen Prozesse der jeweiligen Einrichtung integriert werden kann.

## Voraussetzungen

Zur Nutzung der ADAM API benötigen Sie einen Serviceaccount. Diesen können Sie anfordern, indem sie eine Mail an servicedesk@itc.rwth-aachen.de schicken. Derzeit ist unsere Schnittstelle mit der Windows Powershell 5.1 getestet.

## Inhalt

Beispielprojekt - Dieses Verzeichnis beinhaltet eine Demoanwendung, die die Verwaltung des Active Directories über eine editierbare Excel Datei steuert.

## Autoren
- Christian Maintz
- Jannis Hahn

